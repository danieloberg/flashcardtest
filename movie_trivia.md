# Movie Trivia

Write a motivating rationale of your purpose of your study here.

-----

> Where does the symbols in the green code in The Matrix originate from?

> A sushi cookbook

[🔗](fulldeck://5B2E8017-18DD-464B-9F75-1E6C03796155)

-----

> `Psycho` was the first movie to show a toilet flushing.

[🔗](fulldeck://87591276-CB7D-4C2E-8409-F31C2A1C98F8)

-----

> In which scene, in Alien, where the actors not told what was to happen, so their
> absolutely horrified reactions where real?

> The chestbuster scene

[🔗](fulldeck://215D9234-827A-4742-86D2-3CBA49C5C20B)

-----
